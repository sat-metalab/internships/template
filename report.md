---
title: Report / Rapport
author:
- Firstname Lastname (Intern)
- Firstname(s) Lastname(s) (Supervisor(s))
institute:
- Société des Arts Technologiques [SAT] - metalab
bibliography:
- biblio.bib
- metalab/bib/metalab-papers.bib 
- metalab/bib/metalab-presentations.bib
---

## Abstract

## Résumé

## Introduction

## Related work

### At SAT metalab

Tools from SAT metalab to review in this report may include:

- Haptic Floor [@2019bouillot]
- SATIE [@2016settel]
- Splash [@2021durand;@2019Ouellet]
- ...

## Conclusion

## Acknowledgements / Remerciements

## References / Références
