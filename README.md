# SAT-metalab Internship Template Repository

* Intern: Firstname Lastname
* Supervisor(s): Firstname(s) Lastname(s)

## Description

<!-- link post from https://sat-metalab.gitlab.io/lab/categories/internships/ -->

<!-- copy/paste Objective and Tasks from post below, then later transform into Abstract-->
### Objectives / Objectifs

The main objective of this internship is...

### Tasks / Tâches

 * ...

## Contributions

<!-- List hyperlinks to main contributions: such as documentation and software repositories -->

* ...

## Documents

<!-- Some of these documents may be optional, as defined by each internship -->

* [Presentation / Présentation](presentation.md) 
* [Report / Rapport](report.md)

## Media

* Please use media file formats defined in [.gitattributes](.gitattributes) or update the list with new formats before versioning your media files.
* Please version your media files in folder [media](media), ideally in a sub-folder with the date of versioning in `yyyy-mm-dd` format, for example `media/2022-03-01/`.

## Feedback

Please note that interns and mentors can provide feedback (for instance for mid-term reviews) using our [gitlab issue template](.gitlab/issue_templates/feedback.md) by [following this guide](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates).