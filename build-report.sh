#!/bin/bash

mkdir -p build

cp -r media build/

cp report.css build/

pandoc -s --metadata=link-citations:true --citeproc -t html --css report.css report.md -o build/report.html
# Bibliographies are listed in report.md metadata
# Alternatively they can be passed as parameters to pandoc
# --bibliography 1.bib --bibliography file2.bib # ...