#!/bin/bash

# Fail on errors
set -e

branch="update/template/`date +%Y-%m-%d-%H-%M`"
origin=`git remote get-url origin`

git remote -v | grep template || git remote add template git@gitlab.com:sat-metalab/internships/template.git
git fetch --all
git checkout main
git pull
git checkout -b ${branch}
git merge template/main
git push --set-upstream origin ${branch}

repo=`echo $origin | sed 's/git@gitlab.com:/https:\/\/gitlab.com\//g' | sed 's/\.git//g'`
xdg-open ${repo}/-/merge_requests
