#!/bin/bash

mkdir -p build

cp -r media build/

if [ ! -d build/reveal.js ]
then
    pushd build
    reveal_version="4.2.1"
    wget https://github.com/hakimel/reveal.js/archive/$reveal_version.tar.gz
    tar -xzvf $reveal_version.tar.gz
    mv reveal.js-$reveal_version reveal.js
    rm $reveal_version.tar.gz
    popd
fi

cp presentation.css build/

pandoc -t revealjs --css presentation.css --metadata pagetitle="Internship Presentation" \
  -s -o build/presentation.html -V revealjs-url=reveal.js \
  --variable transition='none' --variable theme="moon" ./presentation.md