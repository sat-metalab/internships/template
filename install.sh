#!/bin/bash

if ! command -v git &> /dev/null
then
    echo "Installing git"
    sudo apt install git -y
fi

echo "Updating git submodules"
git submodule update --init --recursive

if ! command -v git-lfs &> /dev/null
then
    echo "Installing git-lfs"
    sudo apt install git-lfs -y
    git lfs install
fi

echo "Fetching git large files"
git lfs fetch -X 2???*.pdf && git lfs pull -X 2???*.pdf

if ! command -v wget &> /dev/null
then
    echo "Installing wget"
    sudo apt install wget -y
fi

if ! command -v pandoc &> /dev/null
then
    echo "Installing pandoc"
    wget https://github.com/jgm/pandoc/releases/download/2.16.2/pandoc-2.16.2-1-amd64.deb
    sudo dpkg -i pandoc*.deb
fi

echo "Done"
